
SYSeg - System Software by example.

Copyright 2021 (c) by Monaco F. J. <monaco@usp.br>

SYSeg is distributed under the terms of the GNU General Public License v3.

 SYSeg Manual
==============================

TL;DR

    	
- As part of my undergraduate research program, I did some work on 
		scientific instrumentation at the Institute of Physics, where 
    there was this huge and intimidating high-tech microscopy equipment 
    that mainly resembled some alien artifact from a scifi movie. 
    A stainless-steel tag attached to it read:

  "_If all attemps to assemble this device fail, read the instruction manual._"


INTRODUCTION
------------------------------

SYSeg (System Software by example) is a collection of code examples and programming
exercises intended for illustration of general concepts and techniques concerning
system software design and implementation. It may be useful as a companion 
resource for students and instructors exploring this field.

SYSeg is free software :-D

BASIC INFORMATION
------------------------------

Each subdirectory in the source tree has a file `README` wich contains important
instructions on how use the directory's contents. 

Please, as the the name sugests, do read the information --- that will guaranteedly save you time.

If you find a file named `README.m4`, this is not the file you're looking for; 
it's a source file that should be used to create the corresponding `README`.
If you find the source `m4` file but no respective `README` file in the same
directory, this is because you haven't performed the project's configuration procedure described bellow. In this case, please go through the setup instructions and proceed as explained.


SETUP INSTRUCTIONS
------------------------------
 
Some examples in SYSeg need auxiliary artifacts which must be built beforehand.

If you have obtained the project source from the __version control repository__,

i.e. you've cloned the project from its official Git repository, execute the script 

 ```
 $ ./bootsrap.sh
 ```

to create the build configuration script `configure`. To that end, you'll 
need to have GNU Build System (Autotools) installed. In Debian/Ubuntu based 
platforms,  you may install the required software with

```
$ sudo apt install automake autoconf
```

On the other hand, if you have obtained the software form a __distribution 
repository__, usually as a tarball, you should have the  script `configure`
already built, and therefore you may safely skip the previous step.

Either way, locate the file in the root of source directory tree and execute it

```
 $ ./configure
```

This script shall perform a series of tests to collect data about the build 
platform. If it complains about missing pieces of software, install them 
as needed and rerun `configure`. The file `syseg.log` contains a summary
of the last execution of the configuration script.

Finally, build the software with

```
 $ make
 ```

**Note** that, as mentioned, this procedure is intended to build the auxiliary tools needed by SYSeg. The examples and programming exercises themselves will not be built as result of the main configuration script being executed. The process of building the example code is part of the skills those are meant 
to exercise.

To build each example or programming exercise, one need to acess the directories containing the respective source code and follow the instructions indicated in the included README file.

For more detailed instructions on building SYSeg, please, refer to file `INSTALL` in the root of the source tree.

 CONFIGURATION OPTIONS
 ------------------------------

 These are some options you may want to configure.

 If you make any changes, remember to rebuild SYSeg as described above.

 __Configure diff tool__
 
   SYSeg uses a graphical diff tool to help visual comparison among code artifacts.
   You may choose which tool you prefer by editing

      DIFF_TOOL = meld

  in syseg/tools/makefile.utils

  SYSeg CONTENTS
  -------------------------------


 - Directory `eg` contains source code examples.
 - Directory `try` contains programming exercises.
 - Directory `extra` contains non-standard features, advanced topics, and hacker ore.
 - Directory `tools` contains auxiliary tools used by examples and exercises.
 - Directory `doc` contains SYSeg documentation.

  
CONTRIBUTING
-----------------------------------

Bug reports and suggestions are always welcome!

Please contact the author.