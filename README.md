# SYSeg - System Software by Example

SYSeg (System Software by example) is a collection of code examples and programming
exercises intended for illustration of general concepts and techniques concerning
system software design and implementation. It may be useful as a companion 
resource for students and instructors exploring this field.


## Quick start

- Some code examples and programming exercises require auxiliary artifacts 
(including documentation) which must be built beforehand. 

  In order to get thos items built you must perform the setup procedure.

  Please, read `doc/syseg-manual.md' and proceed as explained.

  There you will also find instructions on how to use SYSeg code examples.
 

- For copyright and licensing information, please refer to the file 'COPYING' at the root of the
  project's source tree.

## Overview

SYSeg contents. 

- Directory `eg` contains source code examples.
- Directory `try` contains programming exercises.
- Directory `extra` contains non-standard features, advanced topics, and hacker lore.
- Directory `tools` contains auxiliary tools used by examples and exercises.
- Directory `doc` contains SYSeg documentation.


