
dnl  Makefile.m4 - Makefile template.
dnl    
dnl  Copyright (c) 2021 - Monaco F. J. <monaco@usp.br>
dnl
dnl  This file is part of SYSeg. 
dnl
dnl  SYSeg is free software: you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation, either version 3 of the License, or
dnl  (at your option) any later version.
dnl
dnl  SYSeg is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with .  If not, see <http://www.gnu.org/licenses/>.
dnl
dnl  ------------------------------------------------------------------
dnl
dnl  Note: this is a source file used to produce either a documentation item,
dnl        script or another source file by m4 macro processor. If you've come
dnl	   across a file named, say foo.m4, while looking for one of the
dnl	   aforementioned items, changes are you've been looking for file foo,
dnl	   instead. If you can't find foo, perhaps it is because you've missed
dnl 	   the build steps described in the file README, found in the top 
dnl	   source directory of this project.
dnl        
dnl
include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

bin = decode

# bin = cypherbin 
# main_obj = main.o
# main_lib = libfoobar
# main_ldflags = -L. -Wl,-Bstatic
# cypherbin_obj = cypherbin.o

decode_obj = decode.o cypher.o

# lib = libfoobar
# libfoobar_obj = foo.o

CPP_FLAGS= -Wall 
C_FLAGS= -m32 -fcf-protection=none -O0
LD_FLAGS= -m32

LOCAL_CLEAN = *~

DOCM4_RELEVANT_RULES

DOCM4_MAKEGYVER

UPDATE_MAKEFILE

DOCM4_MAKE_BINTOOLS


# cypher.o : cypher.s
# 	$(CC)  $(MKG_CPPFLAGS) $(MKG_CFLAGS) -fno-pic $($*_cppflags) $($*_cflags) -c $< -o $@

# cypher.s : cypher.c
# 	$(CC)  $(MKG_CPPFLAGS) $(MKG_CFLAGS) -fno-pic $($*_cppflags) $($*_cflags) -S $< -o $@


