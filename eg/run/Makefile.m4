
dnl  Makefile.m4 - Makefile template.
dnl    
dnl  Copyright (c) 2021 - Monaco F. J. <monaco@usp.br>
dnl
dnl  This file is part of SYSeg. 
dnl
dnl  SYSeg is free software: you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation, either version 3 of the License, or
dnl  (at your option) any later version.
dnl
dnl  SYSeg is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with .  If not, see <http://www.gnu.org/licenses/>.
dnl
dnl  ------------------------------------------------------------------
dnl
dnl  Note: this is a source file used to produce either a documentation item,
dnl        script or another source file by m4 macro processor. If you've come
dnl	   across a file named, say foo.m4, while looking for one of the
dnl	   aforementioned items, changes are you've been looking for file foo,
dnl	   instead. If you can't find foo, perhaps it is because you've missed
dnl 	   the build steps described in the file README, found in the top 
dnl	   source directory of this project.
dnl        
dnl
include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

bin = eg-01 eg-02 eg-02_aux eg-03 eg-04 eg-05-rel eg-05-pic

CPP_FLAGS= -Wall -I. $(CPPFLAGS)
C_FLAGS= -O0 -g -m32 -m32 -fcf-protection=none -fcf-protection=none $(CFLAGS)
LD_FLAGS= -L. -m32 $(LDFLAGS)


eg-01 : eg-01.c
	gcc $(CPP_FLAGS) $(C_FLAGS) $(LD_FLAGS) $< -o $@

eg-02 eg-02_aux: % : %.c
	gcc $(CPP_FLAGS) $(C_FLAGS) $(LD_FLAGS) $< -o $@

eg-03 : eg-03.c
	gcc $(CPP_FLAGS) $(C_FLAGS) $(LD_FLAGS) $< -o $@

eg-04.o : eg-04.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fno-pic $< -o $@

eg-04 : eg-04.o
	gcc $(LD_FLAGS) $< -o $@

eg-05.o : eg-05.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fno-pic $< -o $@

eg-05-rel.o : eg-05.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fno-pic $< -o $@


eg-05-aux-rel.o : eg-05-aux.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fno-pic $< -o $@

eg-05-aux-pic.o : eg-05-aux.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fpic $< -o $@

libeg-05-aux.a : eg-05-aux-rel.o
	ar rcs $@ $<

eg-05-static: eg-05.o libeg-05-aux.a
	gcc $(LD_FLAGS) eg-05.o -Wl,-Bstatic -leg-05-aux -Wl,-Bdynamic -o $@

libeg-05-aux-rel.so : eg-05-aux-rel.o
	gcc -m32 --shared -fno-pic $< -o $@

eg-05-rel: eg-05.o libeg-05-aux-rel.so
	gcc $(LD_FLAGS) eg-05.o -leg-05-aux-rel -fno-pic -fno-pie -o $@

eg-05-aux-pic.o : eg-05-aux.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fpic $< -o $@

eg-05-pic.o : eg-05.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) -O1 -fpie $< -o $@

eg-05-pic : eg-05-pic.o libeg-05-aux-pic.so
	gcc $ $(LD_FLAGS) $< -o $@ -leg-05-aux-pic

libeg-05-aux-pic.so : eg-05-aux-pic.o
	gcc -m32 --shared $< -o $@

libother.so : other.c
	gcc -m32 --shared $< -o $@


.PHONY: clean
clean:
	rm -f $(bin)
	rm -f *-rel *-static *.a *.so *.d 


UPDATE_MAKEFILE

DOCM4_MAKE_BINTOOLS

# Local rules

# ps :
# 	term=$$(ps -p $$$$ -o tty=); while true; do ps axf | grep "$$term" ; sleep 1; clear; done 


ps :
	while true; do ps axf | grep "$(TERM)" ; sleep 1; clear; done 


