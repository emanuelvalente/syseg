#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, const char **argv)
{
  int count;

  if (argc>1)
    count = atoi (argv[1]);
  else
    exit (1);

  while (count>0)
    {
      printf ("Program 1: %d\n", count);
      sleep (1);

      if (count == 10)
        execvp ( "./eg-02_aux", (char* []) {"eg-02_aux", "5", NULL} );


      count--;
    }

  return 0;
}
