const char msg[]  = "Hello world";

void __attribute__((naked)) _start()   
{


__asm__("\
        mov   $0x0e,%ah               \n \
        mov   $0x0, %bx               \n \
loop:                                 \n \
        mov   msg(%bx), %al           \n \
        cmp   $0x0, %al               \n \
        je    halt                    \n \
        int   $0x10                   \n \
        add   $0x1, %bx               \n \
        jmp   loop                    \n \
  halt:                               \n \
        hlt                           \n \
        jmp   halt                    \n \
");

}


 

__asm__(".fill 510 - (. - _start), 1, 0");  
__asm__(".byte 0x55, 0xaa");     
