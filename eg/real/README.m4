include(docm4.m4)

 REAL MODE PROGRAMMING
 ==============================

DOCM4_DIR_NOTICE

  Overview
 ------------------------------

 This directory contains a series of examples of accessing the segmented
 memory in x86 rea-mode.

DOCM4_INSTRUCTIONS

 
 Contents
 ------------------------------


 Take a look at the following examples, preferably in the suggested order.


 * eg-00.S   	    Write a character character on the video display memory

   		    The character is referred to by an immediate value.

 * eg-01_alpha. S   Attempt to write from memory to video display memory

   		    We are gradually taking steps to write a string... but for
		    now, to keep things simple, we're staring with the first
		    character only.

		    We try to address the character as a memory location.
		    
  		    The thing is, this code does not work, though.

		    Take a look at the source and try to spot the problem.

 * eg-01.S	    Like its alpha release, but with the problem fixed.

   		    We use the extra segment register to handle the issue.

  
 * eg-02.c          This is a port of eg-01.S in C.

   		    We inicialize segment registers in rt0.c.

		    Function print() uses inline assembly.

 * eg-03.c	    Like eg-02.c, but with print() written in plain C.

   		    Since what we do is simply write onto memory, we can use
		    the regular C constructs of variable attribution; no
		    assembly need in the function implementation. 



 Notes
 ------------------------------

 (1)   Original PC's BIOS used to read the MBR from the first 512 bytes of
       either the floppy or hard disks. Later on, when CD-ROM technology
       came about, it brought a different specification for the boot
       information, described in the iso9960 (the conventional CD file system
       format). Vendors then updated the BIOSes to detect whether the storage
       is either a floppy (or HD) or a CD-ROM, and apply the appropriate boot
       procedure. More recently, when USB flash devices were introduced, the
       approach adopted by BIOS vendors was not to specify a new boot procedure,
       but to emulate  some of the former devices. The problem is that this
       choice is not very consistent: some BIOSes would detect a USB stick as
       a floppy, whereas other BIOSes would see it as a CD (welcome to the
       system layer!).

       If your BIOS mimics the original PC, to make a bootable USB stick
       all you need to do is to copy the MBR code to its first 512 bytes:


   	  make stick IMG=foo.bin STICK=/dev/sdX


       On the other hand, if your BIOS insists in detecting your USB stick
       as a CD-ROM, you'll need to prepare a bootable iso9660 image as
       described in El-Torito specification [1]. 

       As for that, the GNU xorriso utility may come in handy: it prepares a
       bootable USP stick which should work in both scenarios. Xorriso copies
       the MBR code to the first 512 bytes to the USB stick and, in addition,
       it also transfer a prepared iso9660 to the right location. If you can't
       get your BIOS to boot and execute the example with the above (floppy)
       method, then try

         make stick IMG=foo.iso STICK=/dev/sdX


       We wont cover iso9660 El-Torito boot for CD-ROM, as newer x86-based
       computers now offers the Unified Extensible Firmware Interface meant
       to replace the BIOS interface of the original PC.  EFI is capable
       of decoding a format device, allowing the MBR code to be read from a
       standard file system. Although EFI is gradually turning original PC
       boot obsolete, however, most present day BIOSes offer a so called
       "legacy mode BIOS" and its useful to understand more sophisticated
       technologies, as low-level boot is still often found in legacy
       hardware and embedded systems.


  (2)   Original i8086 architecture did not have 32-bit di (edi) register.       
        The only way to reach b800:0000 begin at %ds=0 would be to load          
        0xb800 into %ds or %es.                                                  

	This is what we do in egx-01.S

        Wen we boot a modern x86 nowadasy, it emulates an i8086 hardware         
        but its still an i386 CPU. A 32-bit %edi register does exists            
        and the emulation allow us to load 32-bit values into it. The            
        segments %ds and %es wtill work, though.                                 
                                                                                 
        It is therefore one may take advantage of this to access memory          
        beyond the scope of the current 64k segment.

	This is what we do in egx-02.S

	You can compare both versions with

	    make diff egx-01.bin egx-02.bin BIT=32

	That said, this is not portable to original i8086 and, arguably,
	in a strict sense not plain 16-bit real-mode programming.            


DOCM4_BINTOOLS_DOC


 References
 ------------------------------
 
 [1] Auxiliary program: syseg/src/hex2bin

 [2] El-Torito: https://wiki.osdev.org/El-Torito
