;; Fastcall calling convention                                                                                                                                                                        
;; Quite time and memory efficient; not much flexible, though.
                                                                                                                                                  

global _start

section .text

_start:
        mov ecx, 5              ; Copy values into general-purpose registers                                                                                                                                   
        mov edx, 2
        call my_func            ; Call function                                                                                                                                                                

        mov ebx, eax            ; Exit with computed value                                                                                                                                                     
        mov eax, 1
        int 0x80

my_func:
        mov eax, ecx    		; Assume parameters in registers                                                                                                                                       
        sub eax, edx
        ret
