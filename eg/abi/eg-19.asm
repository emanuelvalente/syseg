	;; A hand-made assembly program                                                                              
       ;; foo(x) = x+1, main returns foo(2)                                                                                       

        global _start
        global main
        global foo

foo:
        push   ebp
        mov    ebp,esp
        mov    eax,DWORD  [ebp+0x8]
        add    eax,0x1
        mov    esp,ebp
        pop    ebp
        ret

main:
        push   ebp
        mov    ebp,esp
        push   0x2
        call   foo
        add    esp,0x4
        mov    esp,ebp
        pop    ebp
        ret


_start:
        call main

        mov ebx, eax
        mov eax, 1
        int 0x80

