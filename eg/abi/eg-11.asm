;; Stack method                                                                                          
;; Version 2: an ad hoc way to restore the stack pointer                                       
                                                           

global _start

section .text

_start:

        push 2                  ; Push second operand onto the stack                                             
        push 5                  ; Push first operand onto the stack                                              
        call my_func            ; Pushes return address and jump to my_func                                      

        mov ebx, eax            ; Exit with computed value                                                       
        mov eax, 1
        int 0x80

my_func:
        mov eax, [esp + 4]      ; Read second operand                                                             
        mov ebx, [esp + 8]      ; Read first operand                                                             
        sub eax, ebx            ; Sub in eax, the return register 
                                               
        pop ebx                 ; pop return address                                                             
        pop ecx                 ; pop first operand                                                              
        pop ecx                 ; pop second operand
        push ebx                ; push return address 
        
        ret
