
        ;; A minimal running example                                                     

        global foo

        section .text            ; Text (program code) section

foo:	        	         ; The entry point 
        mov ebx, 42              ; Exit status is returned to the kernel in ebx 
        mov eax, 1               ; Prepare to call syscall 1 (exit)
        int 0x80		 ; Perform syscall (kernel will check ebx)
