;; C Declarations  - cdecl                                                                                        
;; Caller cleans the stack                                      
                                                           
global _start

section .text

_start:
        push 2         ; Push second operand                                             
        push 5         ; Push first operand                                              
        call my_func   ; Pushes return address and jump                                     

        add esp, 8     ; Clean the stack
        mov ebx, eax   ; Exit with computed value                                                       
        mov eax, 1
        int 0x80        

my_func:
        push ebp       ; Save original ebp
        mov ebp, esp   ; Save original esp                                                              
        add esp, 4     ; Skip the return address     

        pop eax	   ; Read first operand                                                             
        pop ecx	   ; Read first operand                                                             
        sub eax, ecx   ; Sub in the return register
                                               
        mov esp, ebp   ; restore original esp              
        pop ebp        ; Restore original ebp

        ret





