include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

bins_c32 = eg-00 eg-01 eg-02 eg-02_pack eg-04 eg-05 eg-07 eg-06a eg-16 
bins_c64 = eg-04_64_buggy eg-04_64 
bins_nasm32 = eg-03 eg-06 eg-08 eg-09 eg-10 eg-11 eg-12 eg-13 eg-14 eg-15 eg-17 eg-19
bins_as32 = 

sbins = eg-03_beta eg-03_c eg-18 eg-18_fastcall eg-20

targets = $(bins_c32) $(bins_c64) $(bins_nasm32) $(bins_gas32) $(sbins)

all : $(targets) 

UPDATE_MAKEFILE



###########################################################
##
## These are the rules of interest in this set of examples.
##

# When applicable, we use some options to minimize clobbering of asm code
#
#  -fno-pic	disable position-independent code, a default gcc feature meant
#		for building shared libraries (DLL). This is not our case here.
#
#  -fno-assuncrhonous-unwind-tables      disables stack unwinding tables, a 
#					 default gcc feature meant for
#		clearing the stack upon the occurrence of asynchronous events
#		such as exception handling and garbage collection. This is
#		only meaningful if asynchronous execution flow deviations are
#		to be supported. None is the case of our programs here.
#  
#  -fcf-protection=none			  disables code for control-flow
#					  integrity enforcement, a default
#		gcc feature intended to enhance security against return
#		return/call/jump-oriented programming attacks. We can 
#		safely get along without it for benefit of readability.
#
#  -Qn		prevents gcc from outputing compiler metainformation e.g.
#		the section .comment, which is not relevant in this context.

CFLAGS_00 = -m32 -Qn -Wall -Wno-unused-but-set-variable -O0 -fno-pic -fno-pie -fcf-protection=none -fno-asynchronous-unwind-tables
LDFLAGS_00 = -m32 -Wall

# C, x86


eg-02.o eg-02 eg-02_pack.o eg-02_pack : CFLAGS+=-O1

$(bins_c32:%=%.o)  : %.o : %.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(bins_c32) : % : %.o %_aux.o
	gcc  $(LDFLAGS_00) $(LDFLAGS) $(filter %.o, $^) -o $@

%_aux.o :
	@if test -f $(@:%.o=%.c); then\
	 gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -c $(@:%.o=%.c) -o $@;\
	else\
	 touch $@;\
	fi

# C, x86_64

$(bins_c64:%=%.o) : %.o : %.c
	gcc  $(CFLAGS_00) -c $< -o $@

$(bins_c64) : % : %.o
	gcc  $(LDFLAGS_00) $< -o $@

# NASM, x86

$(bins_nasm32:%=%.o) : %.o : %.asm
	nasm -f elf32 $< -o $@

$(bins_nasm32) : % : %.o
	ld -m elf_i386 $< -o $@

$(bins_as32) : % : %.S
	gcc -m32 $< -nostartfiles -nostdlib -o $@

# Special bins

eg-03_beta.o: %.o : %.asm
	nasm -f elf32 $< -o $@

eg-03_beta: eg-03_beta.o
	ld -m elf_i386 -efoo $< -o $@

eg-03_c.o : eg-03_c.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -O1 -masm=intel -c $< -o $@

eg-03_c : % : %.o
	gcc  $(LDFLAGS_00) $(LDFLAGS) -nostartfiles $(filter %.o, $^) -o $@

eg-03_c2 : eg-03_crt0.o eg-03_c2.o 
	gcc  $(LDFLAGS_00) $(LDFLAGS) -nostartfiles $(filter %.o, $^) -o $@

eg-03_c2.o : eg-03_c2.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -O1 -masm=intel -c $< -o $@

eg-03_crt0.o : eg-03_crt0.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -O1 -masm=intel -c $< -o $@

eg-03_c3.o : eg-03_c3.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -O1 -c $< -o $@

eg-03_c3 : eg-03_c3.o 
	gcc  $(LDFLAGS_00) $(LDFLAGS) $(filter %.o, $^) -o $@

eg-03_c : % : %.o
	gcc  $(LDFLAGS_00) $(LDFLAGS) $(filter %.o, $^) -o $@

eg-18: eg-18.o eg-18_sum.o
	gcc  $(LDFLAGS_00) $(LDFLAGS) $(filter %.o, $^) -o $@

eg-18_fastcall: eg-18_fastcall.o eg-18_sum_fastcall.o
	gcc  $(LDFLAGS_00) $(LDFLAGS) $(filter %.o, $^) -o $@

eg-18.o eg-18_fastcall.o : %.o : %.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

eg-18_sum.o eg-18_sum_fastcall.o: %.o : %.asm
	nasm -f elf32 $< -o $@

eg-20 : eg-20.o
	gcc  $(LDFLAGS_00) $(LDFLAGS) $(filter %.o, $^) -o $@

eg-20.o : eg-20.s
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

eg-20.s : eg-20.c
	gcc  $(CFLAGS_00) $(CPPFLAGS) $(CFLAGS) -S $< -o $@


.PHONY: clean
clean:
	rm -f $(targets) *.o *.s


DOCM4_MAKE_BINTOOLS
