	;; Sum up two arguments
       ;; cdcl version

	global sum

	section .text

sum:
	push ebp
	mov ebp, esp
	add esp, 8

	mov eax, [esp]
	add eax, [esp+4]

	mov esp, ebp
	pop ebp
	ret
