include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Make script])

init: all 

UPDATE_MAKEFILE

##
## Relevant rules
##

bin =  eg-01 eg-02 eg-03 eg-04 eg-05 eg-06 eg-07 eg-08 eg-09 eg-10 eg-11

all: $(bin)

eg-01 : eg-01.c
	gcc -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-stack-protector -mpreferred-stack-boundary=2 $<  -o $@

eg-02 : eg-02.c
	gcc -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-stack-protector -mpreferred-stack-boundary=2 $<  -o $@

eg-03 : eg-03.c
	gcc -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-stack-protector  $<  -o $@

eg-04 : eg-04.c
	gcc -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-stack-protector  $<  -o $@

eg-05 : eg-05.c
	gcc -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie $<  -o $@

eg-06 : eg-06.c
	gcc -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie $<  -o $@

eg-07 : eg-07.c
	gcc -Wall -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-pic $<  -o $@

eg-08 : eg-08.c
	gcc -Wall -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-pic $<  -o $@

eg-09 : eg-09.c
	gcc -Wall -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-stack-protector -fno-pie -fno-pic $<  -o $@

eg-10 : eg-10.c
	gcc -Wall -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-pie -fno-pic $<  -o $@

eg-11 : eg-11.c
	gcc -Wall -g -m32 -Wall -Wno-unused -O0 -fcf-protection=none -fno-stack-protector -mpreferred-stack-boundary=2 -fno-pie -fno-pic -z execstack $<  -o $@

eg-11.bin : eg-11.S
	as --32 $< -o $@.o
	ld -melf_i386 --oformat=binary -Ttext=0x0 $@.o -o $@

eg-11.elf : eg-11.S
	as --32 $< -o $@.o
	ld -melf_i386  $@.o -o $@


.PHONY: eg-11.in eg-11.sh
eg-11.in : eg-11.bin
	./eg-11.sh $<


%/main: %
	objdump -m i386 --disassemble=main $<


.PHONY: clean

clean :
	rm -f $(bin) *.o *.in *.bin

dnl
dnl Uncomment to include bintools
dnl
dnl
DOCM4_MAKE_BINTOOLS

